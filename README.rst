FreeForestML
============

FreeForestML (formally nnfwtbn -- Neural network framework to be named) is a
Python framework to train neural networks in the context of high-energy physics.
The framework also provides convenient methods to create the typical plots. The
dataset is assumed to be stored in a dataframe.

Links
=====

 * `Documentation <https://freeforestml.readthedocs.io>`_
