*************
API reference
*************

.. autoclass:: freeforestml.Cut
   :members:
   :special-members:

.. autoclass:: freeforestml.Process
   :members:
   :special-members:


.. autoclass:: freeforestml.Variable
   :members:
   :special-members:

.. autoclass:: freeforestml.variable.BlindingStrategy
   :members:
   :special-members:

.. autoclass:: freeforestml.RangeBlindingStrategy
   :members:
   :special-members:

.. autoclass:: freeforestml.CrossValidator
   :members:
   :special-members:

.. autoclass:: freeforestml.ClassicalCV
   :members:
   :special-members:

.. autoclass:: freeforestml.MixedCV
   :members:
   :special-members:

.. autoclass:: freeforestml.Normalizer
   :members:
   :special-members:

.. autoclass:: freeforestml.EstimatorNormalizer
   :members:
   :special-members:

.. autoclass:: freeforestml.HepNet
   :members:
   :special-members:

.. automodule:: freeforestml.plot
	:members:
