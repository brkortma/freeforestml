.. freeforestml documentation master file, created by
   sphinx-quickstart on Tue Jun 25 19:59:43 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

FreeForestML
============

.. toctree::
   :maxdepth: 2
   :hidden:

   Introduction <self>
   Examples
   api_reference

FreeForestML (formally nnfwtbn -- Neural network framework to be named) is a
Python framework to train neural networks in the context of high-energy physics.
The framework also provides convenient methods to create the typical plots. The
dataset is assumed to be stored in a dataframe.

Links
=====

 * `GitLab repository <https://gitlab.cern.ch/fsauerbu/freeforestml>`_
 * :ref:`genindex`
 * :ref:`search`
